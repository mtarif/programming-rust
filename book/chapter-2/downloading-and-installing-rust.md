# دانلود و نصب راست

بهترین شیوه برای نصب راست، استفاده از `rustup`، نصب کننده ی راست است. به این آدرس https://rustup.rs بروید و مراحلی که گفته شده، انجام دهید.

همچنین می توانید در این آدرس https://www.rust-lang.org به بخش `Downloads` رفته و بسته های از پیش تهیه شده برای سیستم عامل گنولینوکس، مک و ویندوز را دانلود کنید. همچنین راست به صورت پیشفرض در برخی توزیع ها نصب شده است. ما استفاده از `rustup` را توصیه می کنیم، چرا که مشابه `RVM` برای روبی یا `NVM` برای نود، یک ابزار برای مدیریت نصب است. برای مثال زمانی که نسخه ای جدید از راست منتشر شود، قادر خواهید بود با تایپ عبارت `rustup update`، به نسخه ی جدید بروزرسانی کنید.

بعد از نصب راست، باید به سه دستور زیر در ترمینال خود دسترسی داشته باشید:

```bash
$ cargo --version
cargo 0.18.0 (fe7b0cdcf 2017-04-24)
$ rustc --version
rustc 1.17.0 (56124baa9 2017-04-24)
$ rustdoc --version
7rustdoc 1.17.0 (56124baa9 2017-04-24)
$
```

علامت `$` علامت خط فرمان است که در ویندوز می تواند `C:\>` یا موارد مشابه دیگر باشد. در دستورات بالا، ورژن نصب شده ی هر کدام از ابزارها را گزارش کرده ایم:

* • `cargo` یک ابزار مدیریت کامپایل راست،  پکیج منیجر و یک ابزار با اهداف عمومی است. با `cargo` می توانید یک پروژه ی جدید بسازید، برنامه تان را بسازید و اجرا کنید و کتابخانه های خارجی که برنامه تان به آن وابسته است، مدیریت کنید.

* • `rustc` کامپایلر راست است. معمولا ما به `cargo` اجازه می دهیم کامپایلر را برای ما فراخوانی کند، اما گاهی اوقات اجرای مستقیم آن برایمان مفید است.

* • `rustdoc` ابزار مستند سازی راست است. اگر شما مستندات را به فرم مناسب در کامنت های کد برنامه ی خود بنویسید، `rustdoc` می تواند آنها را به خوبی به صورت `HTML` قالب بندی کند. مثل `rustc`، معمولا به `cargo` اجازه می دهیم که `rustdoc` را برایمان فراخوانی کند.

برای راحتی، Cargo می تواند یک بسته جدید راست با برخی از متا دیتای استاندارد که به درستی مرتب شده اند،  برای ما ایجاد کند:

```bash
$ cargo new --bin hello
Created binary (application) `hello` project
```

این دستور یک مسیر جدید پروژه به نام `hello` برایمان ایجاد می کند و فلگ `--bin` مشخص می کند که این پروژه، یک فایل اجرایی است و یک کتابخانه نیست. به فایل های سطح اول پوشه ی `hello` دقت کنید:

```bash
$ cd hello
$ ls -la
total 24
drwxrwxr-x 4 mehran mehran 4096 Sep 20 14:22 .
drwxr-xr-x 3 mehran mehran 4096 Sep 20 14:22 ..
-rw-rw-r-- 1 mehran mehran  106 Sep 20 14:22 Cargo.toml
drwxrwxr-x 6 mehran mehran 4096 Sep 20 14:23 .git
-rw-rw-r-- 1 mehran mehran   19 Sep 20 14:22 .gitignore
drwxrwxr-x 2 mehran mehran 4096 Sep 20 14:22 src
$
```

مشاهده می کنیم که Cargo یک فایل به نام `Cargo.toml` برای نگه داری متا دیتای پروژه ایجاد کرده است. در این زمان این فایل محتوای خاصی ندارد:

```bash
[package]
name = "hello"
version = "0.1.0"
authors = ["Mehran Tarif <mrmntf@riseup.net>"]

[dependencies]
```

اگر برنامه ی ما نیاز به سایر کتابخانه ها پیدا کرد، میتوانیم آنها را در این فایل ذخیره کنیم و Cargo فرایند های دانلود، ساختن (بیلد) و به روزرسانی کتابخانه را بر عهده خواهد گرفت. ما فایل `Cargo.toml` را در [فصل ۸](/chapter-8) پوشش داده ایم.

همچنین Cargo پروژه ما را برای استفاده از سیستم کنترل ورژن `git` با ساختن پوشه ی `.git` و فایل `.gitignore` آماده کرده است. می توانید با مشخص کردن فلگ `--vcs none` از این قابلیت صرف نظر کنید.

کد راست در پوشه ی `src` قرار دارد:

```bash
$ cd src
$ ls -l
total 4
-rw-rw-r-- 1 mehran mehran 45 Sep 20 14:22 main.rs
```

به نظر می رسد که Cargo شروع به نوشتن برنامه از طرف ما کرده است. فایل `main.rs` حاوی متن زیر است:

```rust
fn main() {
	println!("Hello, world!");
}
```

در راست، شما حتی نیازی به نوشتن برنامه `"Hello، World!"` ندارید و این برنامه در `boilerplate` ایجاد برنامه ی Cargo وجود دارد: دو فایل و در مجموع ۹ خط.

در هر پوشه ای از برنامه می توانید دستور `cargo run` را برای بیلد و اجرای برنامه، فراخوانی کنید:

```bash
$ cargo run
  Compiling hello v0.1.0 (file:///home/mehran/rust/hello)
    Finished dev [unoptimized + debuginfo] target(s) in 0.10s
      Running `/home/mehran/rust/hello/target/debug/hello`
Hello, world!
$
```

اینجا Cargo کامپایلر راست، `rustc` را فراخوانی کرده و سپس فایل اجرایی ایجاد شده را، در مسیر `target` قرار داده و اجرا کرده:

```bash
$ ls -l ../target/debug
total 4008
drwxrwxr-x 2 mehran mehran    4096 Sep 20 14:45 build
drwxrwxr-x 2 mehran mehran    4096 Sep 20 14:45 deps
drwxrwxr-x 2 mehran mehran    4096 Sep 20 14:45 examples
-rwxrwxr-x 2 mehran mehran 4077820 Sep 20 14:45 hello
-rw-rw-r-- 1 mehran mehran      86 Sep 20 14:45 hello.d
drwxrwxr-x 3 mehran mehran    4096 Sep 20 14:45 incremental
drwxrwxr-x 2 mehran mehran    4096 Sep 20 14:45 native
$ ../target/debug/hello
Hello, world!
$
```

همون طور که انتظار داریم، Cargo می تواند فایل های ایجاد شده را حذف هم بکند:

```bash
$ cargo clean
$ ../target/debug/hello
bash: ../target/debug/hello: No such file or directory
$
```