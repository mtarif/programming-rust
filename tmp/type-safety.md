# امنیت نوع

راست یک زبان "type-safe" است اما منظور ما از عبارت "type safe" چیست؟

عبارت امنیت خوب به نظر میرسد اما واقعا چه چیزی وجود دارد که ما امن نگه میداریم؟

در این بخش به ***«تعریف رفتار نامشخص»*** از استاندارد ۱۹۹۹ زیان برنامه نویسی سی که تحت عنوان *C99* شناخته می شود، نگاهی می کنیم:

> رفتاری که به محض استفاده از یک سازنده ی برنامه ی ناسازگار یا اشتباه و یا یک داده ی اشتباه رخ میدهد و در این استاندارد جهانی (C99) پیشفرض ها را تامین نمی کند.

به برنامه ی سی زیر دقت کنید:
```c
int main(int argc, char **argv) {
	unsigned long a[1];
	a[3] = 0x7ffff7b36cebUL;
	return 0;
}```

بر طبق C99، به دلیل اینکه این برنامه به یک عنصر بیرون دامنه اعضای آرایه *a* دسترسی پیدا می کند، رفتار آن نامشخص است، به این معنی که می تواند هر کاری را انجام دهد. وقتی این برنامه را در لپ تاپ *جیم* اجرا کردیم، خروجی زیر را تولید کرد:

```
undef: Error: .netrc file is readable by others.
undef: Remove password or make file unreadable by others.
```

سپس برنامه کرش کرد. لپ تاپ جیم حتی یک فایل `.netrc` ندارد. اگر خودتان امتحان کنید، احتمالا خروجی، چیزی کاملا متفاوت خواهد بود.

The machine code the C compiler generated for this main function happens to place the array a on the stack three words before the return address, so storing 0x7ffff7b36cebUL in a[3] changes poor main ’s return address to point into the midst of code in the C standard library that consults one’s .netrc file for a password. When main returns, execution resumes not in main ’s caller, but at the machine code for these lines from the library:

```
warnx(_("Error: .netrc file is readable by others."));
warnx(_("Remove password or make file unreadable by others."));
goto bad;
```

In allowing an array reference to affect the behavior of a subsequent return statement, the C compiler is fully standards-compliant. An undefined operation doesn’t just produce an unspecified result: it is allowed to cause the program to do anything at all.

The C99 standard grants the compiler this carte blanche to allow it to generate faster code. Rather than making the compiler responsible for detecting and handling odd behavior like running off the end of an array, the standard makes the programmer responsible for ensuring those conditions never arise in the first place.

Empirically speaking, we’re not very good at that. While a student at the University of Utah, researcher Peng Li modified C and C++ compilers to make the programs they translated report when they executed certain forms of undefined behavior. He found that nearly all programs do, including those from well-respected projects that hold their code to high standards. And undefined behavior often leads to exploitable security holes in practice. The Morris worm propagated itself from one machine to another using an elaboration of the technique shown before, and this kind of exploit remains in widespread use today.

In light of that example, let’s define some terms. If a program has been written so that no possible execution can exhibit undefined behavior, we say that program is well defined. If a language’s safety checks ensure that every program is well defined, we say that language is type safe.

A carefully written C or C++ program might be well defined, but C and C++ are not type safe: the program shown earlier has no type errors, yet exhibits undefined behavior. By contrast, Python is type safe. Python is willing to spend processor time to detect and handle out-of-range array indices in a friendlier fashion than C:

```python
>>> a = [0]
>>> a[3] = 0x7ffff7b36ceb
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
IndexError: list assignment index out of range
>>>
```

Python raised an exception, which is not undefined behavior: the Python documentation specifies that the assignment to a[3] should raise an IndexError exception, as we saw. Certainly, a module like ctypes that provides unconstrained access to the machine can introduce undefined behavior into Python, but the core language itself is type safe. Java, JavaScript, Ruby, and Haskell are similar in this way.

Note that being type safe is independent of whether a language checks types at compile time or at runtime: C checks at compile time, and is not type safe; Python checks at runtime, and is type safe.

It is ironic that the dominant systems programming languages, C and C++, are not type safe, while most other popular languages are. Given that C and C++ are meant to be used to implement the foundations of a system, entrusted with implementing security boundaries and placed in contact with untrusted data, type safety would seem like an especially valuable quality for them to have.

This is the decades-old tension Rust aims to resolve: it is both type safe and a systems programming language. Rust is designed for implementing those fundamental system layers that require performance and fine-grained control over resources, yet still guarantees the basic level of predictability that type safety provides. We’ll look at how Rust manages this unification in more detail in later parts of this book.

Rust’s particular form of type safety has surprising consequences for multithreaded programming. Concurrency is notoriously difficult to use correctly in C and C++; developers usually turn to concurrency only when single-threaded code has proven unable to achieve the performance they need. But Rust guarantees that concurrent code is free of data races, catching any misuse of mutexes or other synchronization primitives at compile time. In Rust, you can use concurrency without worrying that you’ve made your code impossible for any but the most accomplished programmers to work on.

Rust has an escape valve from the safety rules, for when you absolutely have to use a raw pointer. This is called unsafe code, and while most Rust programs don’t need it, we’ll show how to use it and how it fits into Rust’s overall safety scheme in Chapter 21.

Like those of other statically typed languages, Rust’s types can do much more than simply prevent undefined behavior. An accomplished Rust programmer uses types to ensure values are used not just safely but meaningfully, in a way that’s consistent with the application’s intent. In particular, Rust’s traits and generics, described in Chapter 11, provide a succinct, flexible, and performant way to describe characteristics that a group of types has in common, and then take advantage of those commonalities.

Our aim in this book is to give you the insights you need not just to write programs in Rust, but to put the language to work ensuring that those programs are both safe and correct, and to anticipate how they will perform. In our experience, Rust is a major step forward in systems programming, and we want to help you take advantage of it.